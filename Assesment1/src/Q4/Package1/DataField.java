package Q4.Package1;

public class DataField {
	  public int id;
      public String firstName;
      public String lastName;
      public String dob;
      public String college;
      public String branch;
      public long mobile;
      public String address;
      public int pincode;
      public String email;
      
      public DataField(int i, String fname, String lname, String dob, String clg, String br, 
		long ph, String add, int pin, String mail) {
    	  this.id = i;
    	  this.address=add;
    	  this.branch=br;
    	  this.dob=dob;
    	  this.college=clg;
    	  this.email =mail;
    	  this.firstName=fname;
    	  this.lastName=lname;
    	  this.mobile=ph;
    	  this.pincode=pin;    	  
      }
      
      public void display() {
  		System.out.println("id : " + id);        
  		System.out.println("firstName : " + firstName);
  		System.out.println("lastName : " + lastName);
  		System.out.println("Date of Birth : " + dob);     
  		System.out.println("College : " + college);  
  		System.out.println("Branch : " + branch);
  		System.out.println("Phone number : " + mobile);		
  		System.out.println("Address : " + address);
  		System.out.println("Pincode : " + pincode);
  		System.out.println("email ID : " + email);	
  		}
      
      
}