package Q4.Package2;

import java.util.*;
import Q4.Package1.*;

public class Test {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter id : "); 
		int id = sc.nextInt();
		System.out.println("Enter your firstName : ");
		String firstName = sc.next();
		System.out.println("Enter your lastName : " );
		String lastName = sc.next();
		System.out.println("Enter Date of Birth : " );
		String dob = sc.next();
		System.out.println("Enter the College : " );  
		String college = sc.next();
		System.out.println("Enter the Branch : " );
		String branch = sc.next();
		System.out.println("Enter the Phone number : " );	
		long mobile = sc.nextLong();
		System.out.println("Enter the Address : " );
		String address = sc.next();
		System.out.println("Enter the Pincode : " );
		int pincode = sc.nextInt();
		System.out.println("Enter the email ID : " );	
		String email = sc.next();
		System.out.println("Enter the blood group : " );	
		String bloodgrp = sc.next();
		
		DataField d = new DataField(id, firstName, lastName, dob, college, branch, 
				mobile, address, pincode, email);
		d.display();
		
		Generator obj = new Generator(id, firstName, lastName, dob, college, branch, 
				mobile, address, pincode, email, bloodgrp);
		obj.display();
		sc.close();
	}
	
}
