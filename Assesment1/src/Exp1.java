import java.util.Scanner;

/*
 * How do you rotate an array
 * 1. making a temporary array
 * 2. rotating one by one
 */
public class Exp1 {

	//function to rotate the array left
	private static void rotateArray(int n, int[] arr, int rotations) {
		// TODO Auto-generated method stub
		int[] temp = new int[n];
		 
	    int k = 0;
	 
	    for (int i = rotations; i < n; i++) {
	        temp[k] = arr[i];
	        k++;
	    }
	
	    for (int i = 0; i < rotations; i++) {
	        temp[k] = arr[i];
	        k++;
	    }
	 
	    for (int i = 0; i < n; i++) {
	        arr[i] = temp[i];
	    }
	}
		
	//function to display the array elements
	public static void displayArray(int size, int[] arr) {
		for(int i = 0; i < size; i++) {
			System.out.print(arr[i]+ " ");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		//input he size of the array
		System.out.println("Enter the length of the array : ");
		int n  = sc.nextInt();

		// integer array declaration
		int[] arr = new int[n];
		System.out.println("Enter the array elements : ");

		// input array elements 
		for(int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();		
		}

		// input the number of rotations
		System.out.println("Enter the number of rotations : ");
		int rotations = sc.nextInt();
		
		System.out.println("The original array is : ");
		displayArray(n, arr);
		
		//function call to rotate the array
		rotateArray(n,arr,rotations);
		
		System.out.println("\nThe rotated array is : ");
		displayArray(n, arr);
		
		sc.close();
	}

}
