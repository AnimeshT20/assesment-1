import java.util.*;

public class Exp3 {
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the expression : ");
		
		//input expression as String
		String str = sc.nextLine();
		int count = 0, flag = 0;
		for(int i = 0; i < str.length(); i++)
		{
			char ch = str.charAt(i);
			
			if(ch == '(')
				count++;
			else if(ch == ')')
				count--;
			
			// check if the closed parenthesis is before opened.
			if(count < 0 )
			{
				flag = 1 ;
				break;
			}
		}
		
		
		if(flag ==0 && count == 0 )
			System.out.println("Balanced Paranthesis Present");
		else
			System.out.println("Imbalanced Paranthesis present.");
		
		sc.close();
	}
}

