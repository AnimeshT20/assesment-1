import java.util.Scanner;

public class Q3 {
	
	public static void main(String[]args){
		String name;
		int age;
		String country;
		
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter your name :");
		name = sc.next();
		System.out.println("Enter your age :");
		age = sc.nextInt();
		System.out.println("Enter the country :");
		country = sc.next().toUpperCase();
		
		if(age >= 18 && country == "INDIA")
		{
			System.out.println(name + " is eligible to vote.");
		}
		else
		{
			System.out.println(name + " is not Eligible to vote.");
		}
		
		sc.close();
	}

}
