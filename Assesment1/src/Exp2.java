import java.util.Scanner;

public class Exp2 {
	
	/* using HashSet
	 * 	
	 */
		
	/*	static void findIndices(int n, int arr[], int target)
		{
			HashSet<Integer> s = new HashSet<Integer>();
			for (int i = 0; i < n; ++i)
			{
				int temp = target - arr[i];
				
				if (s.contains(temp)) {
					int j =0;
					for( j = 0; j< n; j++) {
						if(arr[j] == temp) {
							break;
						}
					}
					System.out.println(
						"Indices of the pair with the sum " + target + " are [" + j
						+ ", " + i + "]");
				}
				s.add(arr[i]);
			}
		}
	*/
		

	// function to find the indices of the array with sum "target"
	public static boolean findIndices(int n, int arr[], int target)
	{
		boolean flag = false;
		System.out.println("Indices of the pair with the given target sum " + target);

		for (int i = 0; i < (n - 1); i++) {
			for (int j = (i + 1); j < n; j++) {
				if (arr[i] + arr[j] == target) {
					System.out.println("(" + i + ", " 
							+ j + ")");
					flag = true;
				}
			}
		}
		return flag;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		//input he size of the array
		System.out.println("Enter the length of the array : ");
		int n  = sc.nextInt();

		// integer array declaration
		int[] arr = new int[n];
		System.out.println("Enter the array elements : ");

		// input array elements 
		for(int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();		
		}

		// input target value
		System.out.println("Enter the target value : ");
		int target = sc.nextInt();

		// function call to find if indices are present or not
		if(!findIndices(n, arr, target)) {
			System.out.println("do not exist");
		}

		sc.close();
	}

}
