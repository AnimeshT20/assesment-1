class Singleton{
	static Singleton obj = null;
	private Singleton() {

	}
	public static Singleton getObj() {
		if( obj ==null) {
			obj = new Singleton();
		}
		return obj;
	}
}

public class Q5 {
	public static void main(String[] args) {
		Singleton obj = Singleton.getObj();
		Singleton obj1 = Singleton.getObj();
		Singleton obj2 = obj1;
		
		System.out.println(obj.hashCode()+" "+obj1.hashCode()+ " "+obj2.hashCode());

	}
}

