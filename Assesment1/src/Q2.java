import java.util.*;

public class Q2{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int id;
		String name;
		float marks;
		long phone;
		String email;
		double cgpa;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the Student's ID :");
		id = sc.nextInt();
		System.out.println("Enter the Student's name :");
		name = sc.next();
		System.out.println("Enter the Student's contact number :");
		phone = sc.nextLong();
		System.out.println("Enter the Student's email ID :");
		email = sc.next();
		System.out.println("Enter the Student's marks :");
		marks = sc.nextFloat();
		System.out.println("Enter the Student's CGPA :");
		cgpa = sc.nextDouble();
		
		System.out.println("Student id : "+ id +
				"\nStudent Name : "+ name+ "\nPhone Number: "+ 
				phone+ "\nEmail ID : "+ email + "\nMarks : "+
				marks+ "\nCGPA : "+ cgpa);
		sc.close();
	}

}
